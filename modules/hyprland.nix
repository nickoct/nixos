{ config, pkgs, ... }:

{

    programs.hyprland = {
    # Install the packages from nixpkgs
    enable = true;
    # Whether to enable XWayland
    xwayland.enable = true;
  }; 

	environment.systemPackages = with pkgs; [
	alacritty
	hyprland
	hyprpaper
	waybar
	wofi	
	kitty
	gnome.aisleriot
	neofetch
	vim
	git
	rofi
	xfce.xfce4-terminal
	firefox
	pipewire
	pavucontrol
	];


}
