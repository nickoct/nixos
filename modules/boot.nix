{ config, pkgs, ... }:
{

# Systemd Boot Loader
# boot.loader.systemd.boot.enable = true;
# boot.loader.efi.canTouchEfiVariables = true;

# Grub Boot Loader ( With OS-Prober )
boot.loader.efi.canTouchEfiVariables = true;
boot.loader.grub.enable = true;
boot.loader.grub.devices = [ "nodev" ];
boot.loader.grub.efiSupport = true;
boot.loader.grub.useOSProber = true;

}
