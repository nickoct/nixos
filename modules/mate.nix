{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

# Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the MATE Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.mate.enable = true;


}
